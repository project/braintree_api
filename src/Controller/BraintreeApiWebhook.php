<?php

namespace Drupal\braintree_api\Controller;

use Drupal\braintree_api\BraintreeApiService;
use Drupal\braintree_api\Event\BraintreeApiEvents;
use Drupal\braintree_api\Event\BraintreeApiWebhookEvent;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller for handling Braintree API webhooks.
 */
class BraintreeApiWebhook extends ControllerBase {

  /**
   * Drupal\braintree_api\BraintreeApiService definition.
   *
   * @var \Drupal\braintree_api\BraintreeApiService
   */
  protected $braintreeApi;

  /**
   * The Braintree API Logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * The RequestStack service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * Constructs a new BraintreeApiWebhook object.
   */
  public function __construct(BraintreeApiService $braintree_api, LoggerChannelInterface $logger, EventDispatcherInterface $event_dispatcher, ConfigFactoryInterface $config_factory, DateFormatterInterface $date_formatter, RequestStack $stack) {
    $this->braintreeApi = $braintree_api;
    $this->logger = $logger;
    $this->eventDispatcher = $event_dispatcher;
    $this->config = $config_factory->get('braintree_api.settings');
    $this->dateFormatter = $date_formatter;
    $this->requestStack = $stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('braintree_api.braintree_api'),
      $container->get('logger.channel.braintree_api'),
      $container->get('event_dispatcher'),
      $container->get('config.factory'),
      $container->get('date.formatter'),
      $container->get('request_stack')
    );
  }

  /**
   * Handle incoming webhook.
   *
   * @return string
   *   Return Hello string.
   */
  public function handleIncomingWebhook() {
    $request = $this->requestStack->getCurrentRequest();

    $bt_signature = $request->request->get('bt_signature');
    $bt_payload = $request->request->get('bt_payload');

    if (isset($bt_signature) && isset($bt_payload)) {
      try {
        $webhook_notification = $this->braintreeApi->getGateway()
          ->webhookNotification()
          ->parse(
            trim($bt_signature),
            trim($bt_payload)
          );
      }
      catch (\Exception $e) {
        $this->logger->error('Error parsing the Braintree Webhook: ' . $e->getMessage() . '(' . get_class($e) . ')  ' . print_r($bt_signature, TRUE) . '  ' . print_r($bt_payload, TRUE));
        return new Response('Forbidden', Response::HTTP_FORBIDDEN);
      }

      if ($this->config->get('verbose')) {
        $datetime = $webhook_notification->timestamp;
        $date = $this->dateFormatter->format($datetime->getTimestamp(), 'html_datetime');
        if (empty($webhook_notification->subscription)) {
          /** @var \DateTime $date */
          $this->logger->info('Received webhook of kind %kind, dated %date, without an associated subscription', [
            '%kind' => $webhook_notification->kind,
            '%date' => $date,
          ]);
        }
        else {
          $this->logger->info('Received webhook of kind %kind, dated %date, for subscription with id %id', [
            '%kind' => $webhook_notification->kind,
            '%id' => $webhook_notification->subscription->id,
            '%date' => $date,
          ]);
        }
      }
      // Dispatch the webhook event.
      $event = new BraintreeApiWebhookEvent($webhook_notification->kind, $webhook_notification);
      $this->eventDispatcher->dispatch($event, BraintreeApiEvents::WEBHOOK);

      return new Response('Thanks!', Response::HTTP_OK);
    }

    return new Response(NULL, Response::HTTP_FORBIDDEN);
  }

}
